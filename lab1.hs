-- Please do not remove the following lines: 

import qualified Data.Char as DC
import Data.List


{- Exercise 1: Simple functions, lists, tuples, list comprehensions and types

Log into Linux, open a terminal window, create
a suitable directory in your directory tree and change to this directory. Then start GHCi by entering the command
**ghci** at the prompt. Open another terminal, change to the same directory and then open your favourite editor
in the terminal if it is a terminal editor. Otherwise make sure it saves files in the same directory in which you
opened GHCi.

Use the editor to open and start writing a file called **LabSheet1.hs** -}

{- 1.1: square
Consider a function called **square** which squares integers. In the file **LabSheet1.hs** write a type
and a definition for **square**.  -}
square :: Int -> Int 
square x = x * x

{- 1.2: pyth
Consider a function called **pyth** which takes a pair of integers and returns the sum of the squares of the
two integers. Write a type and definition for **pyth** so that it calls **square**.  -}
pyth :: Int -> Int -> Int
pyth x y = square x + square y

{- 1.3: isTriple
Write (with a type declaration) a function **isTriple** that takes three integers and checks whether they
form the sides of a right angled triangle. The last number should be the hypotenuse. Use the function **pyth**.  -}
isTriple :: Int -> Int -> Int -> Bool
isTriple x y z = pyth x y == z * z


{- 1.4: isTripleAny
Improve **isTriple** so that the hypotenuse can be in any position. Call the new function **isTripleAny**.  -}
isTripleAny :: Int -> Int -> Int -> Bool
isTripleAny x y z = (isTriple x y z) || (isTriple x z y) || (isTriple y z x)

{- 1.5: halveEvens
Use the functions **div** and **mod :: Int -> Int -> Int** and list comprehensions to write a function
**halveEvens :: [Int] -> [Int]** which halves each even number in a list. E.g.

halveEvens [1,2,3,4,5,6] == [1,1,3,2,5,3]  -}
halveEvens :: [Int] -> [Int]
halveEvens l = [if x `mod` 2 == 0 then x `div` 2 else x | x <- l]

{- 1.6: inRange
Use list comprehensions to write a function **inRange :: Int -> Int -> [Int] -> [Int]** to return all numbers
in the input list within the range given by the first two arguments (inclusive). For example:

inRange 5 10 [1..15]
== [5,6,7,8,9,10]  -}
inRange :: Int -> Int -> [Int] -> [Int]
inRange min max l = [x | x <- l, x >= min, x <= max]


{- 1.7: countPositives
Write a function **countPositives** to count the positive numbers in a list (the ones strictly greater
than 0). For example:
countPositives [0,1,-3,-2,8,-1,6] == 3  -}
countPositives :: [Int] -> Int
countPositives l = sum([if x > 0 then 1 else 0 | x <- l])


{- 1.8: capitalised
Write a function **capitalised :: String -> String** which, given a word, capitalises it. This means
that the first character should be made uppercase and any other letters should be made lowercase. For example,

capitalised
"mELboURNe" == "Melbourne"

Your definition should use a list comprehension and the library functions **toUpper**
and **toLower** that change the case of a character. Use the internet to find out which library module they are
in and how to load a module.  -}
capitalised :: String -> String
capitalised [] = []
capitalised (first:rest) = DC.toUpper first : [DC.toLower c | c <- rest]

{- 1.9: title
Using the function **capitalised** from the previous question, write a function **title :: [String] -> [String]**
which, given a list of words, capitalises them as a title should be capitalised. The proper capitalisation of a
title (for our purposes) is as follows: the first word should be capitalised. Any other word should be capitalised
if it is at least four letters long. For example:

title ["tHe", "bOSun", "ANd", "thE", "BriDGe"] == ["The", "Bosun",
"and", "the", "Bridge"]

Your function should use a list comprehension, and you will probably need some other auxiliary
functions. You may use library functions that change the case of a character and the function **length**. You will
need to write a recursive definition to cover the case when the string is
empty.  -}

lowered :: String -> String
lowered str = [DC.toLower c | c <- str]

titelify :: String -> String
titelify str = if length str >= 4 then capitalised str else lowered str

title :: [String] -> [String]
title [] = []
title (first:rest) = capitalised first : [titelify str | str <- rest]
