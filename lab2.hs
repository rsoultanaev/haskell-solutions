import Data.Char

inRangeRec :: Int -> Int -> [Int] -> [Int]
inRangeRec min max [] = []
inRangeRec min max (first:rest) = if (first >= min && first <= max) then first : inRange min max rest else inRange min max rest

countPositivesRec :: [Int] -> Int
countPositivesRec [] = 0
countPositivesRec (first:rest) = if first > 0 then 1 + countPositives rest else countPositives rest

capitalisedRec :: String -> String
capitalisedRec [x] = [toUpper x]
capitalisedRec l = capitalisedRec (init l) ++ [toLower (last l)]

-- Helper functions for titleRec
smartCapitalise2 inputs = if length inputs <= 4 then [toLower c | c <- inputs] else capitalisedRec inputs

titleRec :: [String] -> [String]
titleRec [x] = [capitalisedRec x]
titleRec xs = titleRec (init xs) ++ [smartCapitalise2 (last xs)]

insert :: Ord a => a -> [a] -> [a]
insert x [] = [x]
insert head (x:xs) = (if head < x then [head]++[x]++xs else ([x]++ insert head xs))

isort :: Ord a => [a] -> [a]
isort [] = []
isort (x:xs) = insert x (isort xs)

merge :: Ord a => [a] -> [a] -> [a]
merge [] [] = []
merge [] [x] = [x]
merge [x] [] = [x]
merge (x:xs) [] = (x:xs)
merge [] (y:ys) = (y:ys)
merge (x:xs) (y:ys) = (if x < y then [x] ++ merge xs (y:ys) else [y] ++ merge (x:xs) ys)

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge (msort xs1) (msort xs2) where (xs1,xs2) = split xs

split :: [a] -> ([a],[a])
split (x:y:zs) = (x:xs,y:ys) where (xs,ys) = split zs
split xs = (xs,[])

rotor :: Int -> String -> String
rotor num list = (drop num list) ++ (take num list)

makeKey :: Int -> [(Char,Char)]
makeKey num = [("ABCDEFGHIJKLMNOPQRSTUVWXYZ"!!(x-1),(rotor num "ABCDEFGHIJKLMNOPQRSTUVWXYZ")!!(x-1)) | x <- [1..26]]

lookUp :: Char -> [(Char, Char)] -> Char
lookUp char [] = char
lookUp char cs =  if fst (head cs) == char then snd (head cs) else lookUp char (tail cs)

encipher :: Int -> Char -> Char
encipher num letter = lookUp letter (makeKey num)

normalise :: String -> String
normalise input = [ toUpper x | x <- input, (x >= 'a' && x <= 'z') || (x >= 'A' && x <= 'Z') || (x>= '0' && x <= '9')]

encipherStr :: Int -> String -> String
encipherStr num input = [encipher num x | x <- normalise input]

bruteForce :: String -> [String]
bruteForce input = [encipherStr x input | x <- [1..26]]

