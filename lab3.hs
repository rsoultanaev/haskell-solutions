mult :: (Num a) => [a] -> a
mult xs = foldr (\acc x -> acc * x) 1 xs

posList :: (Ord a) => (Num a) => [a] -> [a] 
posList xs = filter (>0) xs

trueList :: [Bool] -> Bool
trueList bs = foldr (\acc x -> x && acc) True bs

evenList :: [Int] -> Bool
evenList xs = foldr (\acc x -> x && acc) True (map (even) xs) 

maxList :: (Ord a) => [a] -> a
maxList xs = foldr (\acc x -> if x > acc then x else acc) (head xs) xs

inRange :: Int -> Int -> [Int] -> [Int]
inRange min max xs = filter p xs where p x = if x <= max && x >= min then True else False

countPositives :: (Ord a) => (Num a) => [a] -> Int 
countPositives xs = length (filter (> 0) xs) 

myLength :: [a] -> Int 
myLength xs = foldr (\acc x -> acc + x) 0 (map (toOne) xs) where toOne x = 1 

myMap :: (a -> b) -> [a] -> [b] 
myMap f xs = foldr (\acc xs -> (f acc):xs)[]xs

myLength2 :: [a] -> Int 
myLength2 xs = foldr (\acc x -> acc + x) 0 (foldr (\acc xs -> (f acc):xs)[]xs) where f x = 1 

